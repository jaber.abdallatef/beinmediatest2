<?php 

namespace App\Services;
use App\Expert ; 
use App\ExpertSlot ; 


class ExpertService {
     
    public function getExpertAvaialbleSlots($request){
         
        $id   = $request->id ; 
        $date = $request->date  ; 
        $start = $request->start  ; 
        $end   =  $request->end  ; 
         $dateTakenSlots = Expert::where([[ 'expert.id' ,'=' , $id],
         ['expert_slot.date','=', $date]])->leftJoin('expert_slot' ,
          'expert.id' , '=' , 'expert_slot.expert_id')->get();
     
        $avArray =  $this->generateAvailibiltyArray($start ,$end);
        if(count($dateTakenSlots) > 0){
            foreach($dateTakenSlots as $slot){
                $avArray[$slot['slot']] = 0 ;
           }  
        }
          //// ex : [1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]
        return $avArray ; 
    }
    public function generateAvailibiltyArray($start , $end){
        /*generate array  : every element equals to 1 
          and count of elements  is countOfSlots :  which is count of small slots (15) 
          between  start and end times */
         $diffMinuts  = (strtotime($end) - strtotime($start)) / 60;
         $countOfSlots  =    $diffMinuts/15 ; 
         $availabiltyArray = [] ; 
         $availabiltyArray =  array_fill(0,$countOfSlots, 1 );
         return  $availabiltyArray ; 
        }
 ////// 
 public function submitBooking($request){
  $expert_id   = $request->expert ; 
  $date = $request->date;
  $startSlot = $request->slot ; 
  $duration = $request->duration ; 
  $username  = $request->name ; 

  $endSlot  = $startSlot * $duration ; 
  
  for($i  = $startSlot ; $i <=  $endSlot; $i++ ){
   $expertSlot  = new   ExpertSlot(); 
   $expertSlot->expert_id = $expert_id; 
   $expertSlot->date   = $date ; 
   $expertSlot->slot = $i ; 
   $expertSlot->user = $username ; 
   $expertSlot->save() ; 
  }
   


 }
///-------------------
 public function getExperts(){
      return Expert::all() ; 
 }


 /////// ------------------
}