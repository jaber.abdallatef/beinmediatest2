<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Expert extends Model
{
    //
    public $table  = "expert";
    public function slots()
    {
        return $this->hasMany('App\ExpertSlot');
    }
}
