<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExpertSlot extends Model
{
    //

    public $table  = "expert_slot";

    public function expert()
    {
        return $this->belongsTo('App\Expert');
    }
}
