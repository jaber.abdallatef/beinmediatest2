<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Expert; 
use App\Services\ExpertService ; 
class ExpertController extends Controller
{
    //

    protected $expertService;
    function __construct(ExpertService $expertService)
    {
        $this->expertService = $expertService;
    }

    public function expertSlots(Request $request)
    {
        
       return  $this->expertService->getExpertAvaialbleSlots($request);
 
    }


    public function expertBooking(Request $request)
    {
        
       return  $this->expertService->submitBooking($request);
 
    }
    

    public function index()
    {
        
       return  $this->expertService->getExperts();
 
    }
}
