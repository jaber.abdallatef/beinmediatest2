<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class ExpertSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        DB::table('expert')->insert([
            [
                'name' => 'William Jordan',
                 'expert' => 'Doctor', 
                 'country' => 'Anabar', 
                 'timezone' => '+12', 
                  'start_working' => '6:00:00',
                  'end_working' => '17:00:00',
            ],
            [
                'name' => 'Quasi Shawa',
                 'expert' => 'Civil engineer', 
                 'country' => 'Syria', 
                 'timezone' => '+3', 
                  'start_working' => '6:00:00',
                  'end_working' => '12:00:00',
            ],
            [
                'name' => 'Shimaa Badawy',
                 'expert' => 'Computer Engineer', 
                 'country' => 'Egypt', 
                 'timezone' => '+2', 
                  'start_working' => '13:00:00',
                  'end_working' => '14:00:00',
            ]]
    );
    }


}
